// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBpZclLk61E-yGW_1yssyJ9wiGyowK8n0g',
    authDomain: 'kp-file-upload.firebaseapp.com',
    databaseURL: 'https://kp-file-upload.firebaseio.com',
    projectId: 'kp-file-upload',
    storageBucket: 'kp-file-upload.appspot.com',
    messagingSenderId: '1011048600893',
    api: 'https://us-central1-ogar-2018.cloudfunctions.net',
  },
  googleMapsKey: 'AIzaSyAiyeaUTZXkbyA5OTQoANKQwNmT1XpfFO4',
  algolia: {
    appId: 'MDGH5IJZUN',
    apiKey: 'fd9e97db99ee26df257df34ae6dccc49',
    indexName: 'items',
    urlSync: false,
  },
  p24: { // todo zmienic na produkcyjne
    testConnection: 'https://sandbox.przelewy24.pl/testConnection',
    trnRegister: 'https://sandbox.przelewy24.pl/trnRegister',
    trnRequest: 'https://sandbox.przelewy24.pl/trnRequest',
    transactionDirect: 'https://sandbox.przelewy24.pl/trnDirect',
    transactionVerify: 'https://sandbox.przelewy24.pl/trnVerify',
  },
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
