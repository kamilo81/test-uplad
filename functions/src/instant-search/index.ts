import * as functions from 'firebase-functions';
import { AlgoliaItem } from '../../../src/app/shared/model/item.model';
import { Change, EventContext } from 'firebase-functions';
import { DocumentSnapshot } from 'firebase-functions/lib/providers/firestore';
import * as admin from 'firebase-admin';
import GeoPoint = admin.firestore.GeoPoint;
import { AlgoliaImageObj, ImageObj, Images } from '../../../src/app/user/shared/models/image.model';
import { cloneDeep } from 'lodash';
import { PaymentStatus } from '../../../src/app/shared/services/payment.service';

const algoliasearch = require('algoliasearch');
const algolia = algoliasearch(functions.config().algolia.appid, functions.config().algolia.adminkey);

export const updateIndex = functions.firestore.document(`items/{itemId}`)
  .onWrite(( snapshot: Change<DocumentSnapshot>, context: EventContext ) => {
    const index = algolia.initIndex('items');
    const itemId = context.params.itemId;
    const item: AlgoliaItem = <AlgoliaItem>snapshot.after.data();
    item.objectID = itemId;
    const geoPoint: GeoPoint = item.localization;
    item._geoloc = {
      lat: geoPoint.latitude,
      lng: geoPoint.longitude,
    };

    if (item.deleteTime || !item.isActive) {
      return index.deleteObject(itemId, ( err: any ) => {
        if (!err) {
          console.log('Item deleted');
        }
      });
    }

    item.images = item.images.map(( imageObj: ImageObj ) => {

      const copyItems: Images = cloneDeep(imageObj.items);
      imageObj.items = {
        original: copyItems[ '320' ] || copyItems[ '640' ] || copyItems[ '828' ] || copyItems[ '1200' ] || copyItems[ 'original' ] || null,
      };


      return <AlgoliaImageObj>imageObj;
    });

    return new Promise(( resolve, reject ) => {
      index.saveObject(item, ( err: any, content: any ) => {
        if (err) {
          reject(err);
          throw err;
        }
        console.log('Items updated' + JSON.stringify(content));
        resolve();
      });
    });
  });

export const deleteIndex = functions.firestore.document(`items/{itemId}`)
  .onDelete(( snapshot: any, context: EventContext ) => {
    const itemId = context.params.itemId;
    const index = algolia.initIndex('items');

    return new Promise(( resolve, reject ) => {
      index.deleteObject(itemId, ( err: any ) => {
        if (!err) {
          console.log('success');
          resolve();
        } else {
          reject(err);
        }
      });
    });
  });
