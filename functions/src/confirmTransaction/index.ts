import { firestore } from 'firebase';
import * as express from 'express';

const express = require('express');
const cookieParser = require('cookie-parser')();
const cors = require('cors')({origin: true});
const app = express();
const bodyParser = require('body-parser');
const request = require('request');
const querystring = require('querystring');

import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { PaymentData, PaymentStatus } from '../../../src/app/shared/services/payment.service';
import { Item } from '../../../src/app/shared/model/item.model';

const db: firestore.Firestore = <firestore.Firestore>admin.firestore();

export interface ConfirmTransactionRequest {
  p24_merchant_id: number;
  p24_pos_id: number;
  p24_session_id: string;
  p24_amount: number;
  p24_currency: string;
  p24_order_id: number;
  p24_sign: string;
}

export interface ConfirmTransactionData extends ConfirmTransactionRequest {
  p24_method: number;
  p24_statement: string;
}

app.use(cors);
app.use(cookieParser);
app.use(bodyParser.json());

// confirm/transaction
app.post('/transaction', ( req: express.Request, res: express.Response ) => {
  const data: ConfirmTransactionData = req.body;
  const userId: string = data.p24_session_id.split('-')[ 0 ];
  const productId: string = data.p24_session_id.split('-')[ 1 ];
  const paymentId: string = data.p24_session_id.split('-')[ 2 ];

  const confirmData: ConfirmTransactionRequest = {
    p24_merchant_id: data.p24_merchant_id,
    p24_pos_id: data.p24_pos_id,
    p24_session_id: data.p24_session_id,
    p24_amount: data.p24_amount,
    p24_currency: data.p24_currency,
    p24_order_id: data.p24_order_id,
    p24_sign: data.p24_sign,
  };

  request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    uri: 'https://sandbox.przelewy24.pl/trnVerify', // todo zmienic na produkcje
    form: {
      ...confirmData,
    },
    method: 'POST',
  }, ( err, response, body ) => {

    if (err) {
      console.log('err ', err);
      return res.status(500).send(err);
    }
    const p24response = querystring.parse(body);

    if (parseInt(p24response.error, 10) !== 0) {
      res.status(500).send(p24response);
    }

    const p1 = db.doc<Item>(`items/${productId}`)
      .update({
        paymentStatus: PaymentStatus.SUCCESS,
      });

    const p2 = db.doc<PaymentData>(`payments/${userId}/payments/${paymentId}`)
      .update({
        status: PaymentStatus.SUCCESS,
        changeDate: new Date(),
      });

    Promise.all([
      p1, p2,
    ]).then(() => {
      res.status(200).send();
    })
      .catch(( error ) => {
        console.log(error);
        res.status(500).send(error);
      });
  });
});

export const transaction: any = functions.https.onRequest(app);
