import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { Change, EventContext } from 'firebase-functions';
import { DataSnapshot } from 'firebase-functions/lib/providers/database';

const firestore = admin.firestore();

export const userPresence = functions
  .database
  .ref('/users/{uid}')
  .onUpdate( (snapshot: Change<DataSnapshot>, context: EventContext ) => {
    const eventStatus = snapshot.after.val();
    const userStatusFirestoreRef = firestore.doc(`usersStatus/${context.params.uid}`);
    const userFirestoreRef = firestore.doc(`users/${context.params.uid}`);
    const userPrivateFirestoreRef = firestore.doc(`users/${context.params.uid}/private/user`);

    return Promise.all([
      userStatusFirestoreRef.set({
        online: eventStatus.status,
        lastSignInTime: new Date(eventStatus.lastOnline),
      }),
      userFirestoreRef.update({
        lastSignInTime: new Date(eventStatus.lastOnline),
      }),
      userPrivateFirestoreRef.update({
        lastSignInTime: new Date(eventStatus.lastOnline),
      })]);
  });
