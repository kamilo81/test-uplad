import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { EventContext } from 'firebase-functions';
import { DocumentSnapshot } from 'firebase-functions/lib/providers/firestore';
import DocumentReference = admin.firestore.DocumentReference;
const firestore = admin.firestore();
export const fcmSend = functions.firestore
  .document(`messages/{userId}/userMessages/{messageId}`)
  .onCreate((snapshot: DocumentSnapshot, context: EventContext ) => {
    const message = <{title: string, body: string, image: string}>snapshot.data();
    const userId = context.params.userId;
    const payload = {
      notification: {
        title: message.title,
        body: message.body,
        icon: message.image,
      },
    };
    const documentRef: DocumentReference = firestore.doc(`fcmTokens/${userId}`);
    return documentRef
      .get()
      .then((ref: any) => {
        return ref.data().token;
      })
      .then((userFcmTokens: string[]) => {
        admin.messaging().sendToDevice(userFcmTokens, payload)
          .then((res: admin.messaging.MessagingDevicesResponse) => {
            const tokensToRemove: string[] = [];

            res.results.forEach((result, index) => {

              const error = result.error;
              if (error) {
                // Cleanup the tokens who are not registered anymore.
                if (error.code === 'messaging/invalid-registration-token' ||
                  error.code === 'messaging/registration-token-not-registered') {
                  tokensToRemove.push(userFcmTokens[index]);
                }
              }
            });
            tokensToRemove.forEach((token) => {
              const index = userFcmTokens.findIndex((currentToken: string) => currentToken === token);
              userFcmTokens.splice(index, 1);
            });

            if (tokensToRemove.length > 0) {
              documentRef.set({token: userFcmTokens})
                .then(() => {
                  return true;
                });
            } else {
              return true;
            }
          })
          .catch((err) => {
            console.log(err);
            return false;
          });
      })
      .catch(err => {
        console.log(err);
        return false;
      });
  });
