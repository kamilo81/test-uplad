import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { ImageObj, Images } from '../../../src/app/user/shared/models/image.model';
import * as firebase from 'firebase';
import QuerySnapshot = firebase.firestore.QuerySnapshot;
import QueryDocumentSnapshot = firebase.firestore.QueryDocumentSnapshot;

const gcs = require('@google-cloud/storage')({keyFilename: 'credentials.json'});
gcs.interceptors.push({
  request: ( reqOpts: any ) => {
    reqOpts.forever = false;
    return reqOpts;
  },
});
const sharp = require('sharp');
const _ = require('lodash');
const path = require('path');
const os = require('os');
const firestore = admin.firestore();

export const generateThumbnail: any = functions.storage.object().onFinalize(object => {
  const fileBucket: any = object.bucket; // The Storage bucket that contains the file.
  const filePath: any = object.name; // File path in the bucket.
  const contentType: any = object.contentType; // File content type.
  const metageneration: any = object.metageneration; // Number of times metadata has been generated. New objects have a value of 1.
  const config = {
    action: 'read',
    expires: '03-01-2500',
  };
  const bucket = gcs.bucket(fileBucket);
  const file = bucket.file(filePath);

  const itemId: string = path.dirname(filePath).split('items/')[ 1 ];
  const imageId: string = filePath.split('/').pop().split('.')[ 0 ].split('++')[ 1 ];

  const order: number = parseInt(filePath.split('/').pop().split('__')[ 0 ], 10);

  const generate: Function = ( size: number ): Promise<any> => {
    let fileName: string;
    let originalFilePath: string;
    if (size === 1200) {
      fileName = filePath.split('/').pop().split('.')[ 0 ];
      originalFilePath = filePath;
    } else if (size === 828) {
      fileName = filePath.split('/').pop().split('.')[ 0 ].split(`_1200_thumb`)[ 0 ];
      originalFilePath = filePath.replace(`_1200_thumb`, '');
    } else if (size === 640) {
      fileName = filePath.split('/').pop().split('.')[ 0 ].split(`_828_thumb`)[ 0 ];
      originalFilePath = filePath.replace(`_828_thumb`, '');
    } else if (size === 320) {
      fileName = filePath.split('/').pop().split('.')[ 0 ].split(`_640_thumb`)[ 0 ];
      originalFilePath = filePath.replace(`_640_thumb`, '');
    }
    const originalFile = bucket.file(originalFilePath);

    const extension = filePath.split('/').pop().split('.').reverse()[ 0 ];
    const tempFilePath = path.join(os.tmpdir(), `${fileName}.${extension}`);
    const newFileName = `${fileName}_${size}_thumb.${extension}`;
    const newFileTemp = path.join(os.tmpdir(), newFileName);

    const fileDir = path.dirname(filePath);
    const thumbFilePath = path.normalize(path.join(fileDir, `${newFileName}`));

    return originalFile.download({
      destination: tempFilePath,
    }).then(() => {

      return new Promise(( resolve ) => {
        sharp(tempFilePath)
          .resize(size, null)
          .toFile(newFileTemp, ( err: any, info: any ) => {

            bucket.upload(newFileTemp, {
              destination: thumbFilePath,
            })
              .then(() => {
                resolve();
              });

          });
      });
    });
  };

  if (!contentType.startsWith('image/')) {
    console.log('This is not an image.');
    return;
  }

  return file.getSignedUrl(config)
    .then(( urls: string[] ) => {
      return firestore.collection(`items/${itemId}/images`)
        .doc(imageId)
        .set({
          id: imageId.split('_')[ 0 ],
          url: urls[ 0 ],
          order: order,
          size: imageId.split('_')[ 1 ] || 'original',
        })
        .then(() => {
          if (_.includes(filePath, '_1200_thumb')) {
            return generate(828);
          } else if (_.includes(filePath, '_828_thumb')) {
            return generate(640);
          } else if (_.includes(filePath, '_640_thumb')) {
            return generate(320);
          } else if (_.includes(filePath, '_320_thumb')) {

            return firestore.collection(`items/${itemId}/images`)
              .orderBy('order')
              .get()
              .then(( querySnapshot: any ) => {
                const tmpImageObj: { [key: string]: ImageObj } = {};

                let index: number = 0;

                (<QuerySnapshot>querySnapshot).forEach((snapshot: QueryDocumentSnapshot) => {
                  const el: { id: string; size: string; order: number; url: string } = <{ id: string; size: string; order: number; url: string }>snapshot.data();
                    if (tmpImageObj[ el.id ]) {
                      tmpImageObj[ el.id ].items[ el.size ] = el.url;
                    } else {

                      const items: Images = {};
                      items[ el.size ] = el.url;

                      tmpImageObj[ el.id ] = {
                        id: el.id,
                        defaultImg: index === 0,
                        items,
                      };
                    }
                  index++;
                });
                return firestore.doc(`items/${itemId}`).set({
                  images: <ImageObj[]>_.values(tmpImageObj),
                }, {
                  merge: true,
                });
              });
          } else {
            return generate(1200);
          }
        })
        .catch(err => {
          console.log(err);
        });
    });

});

