import { firestore, User as FirebaseUser } from 'firebase';
import * as express from 'express';

const express = require('express');
const cookieParser = require('cookie-parser')();
const cors = require('cors')({origin: true});
const app = express();
const bodyParser = require('body-parser');
const md5 = require('md5');
const request = require('request');
const querystring = require('querystring');
import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { PaymentData, PaymentItem } from '../../../src/app/shared/services/payment.service';
import DocumentSnapshot = firestore.DocumentSnapshot;
import { User } from '../../../src/app/shared/model/user.model';


const db: firestore.Firestore = <firestore.Firestore>admin.firestore();

export interface TransactionBody {
  p24_merchant_id?: number;
  p24_pos_id?: number;
  p24_session_id?: string;
  p24_amount?: number;
  p24_currency?: string;
  p24_description?: string;
  p24_email?: string;
  p24_client?: string;
  p24_address?: string;
  p24_city?: string;
  p24_zip?: string;
  p24_country?: string;
  p24_phone?: string;
  p24_language?: string;
  p24_url_return?: string;
  p24_url_status?: string;
  p24_shipping?: number;
  p24_api_version?: string;
  p24_sign?: string;

  [ key: string ]: string | number;
}

const MERCHANT_ID: number = 66860;
const CRC: string = '67dc095f3c899a02';
const gcs = require('@google-cloud/storage')({keyFilename: 'credentials.json'});
gcs.interceptors.push({
  request: ( reqOpts: any ) => {
    reqOpts.forever = false;
    return reqOpts;
  },
});

const validateFirebaseIdToken = ( req: express.Request, res: express.Response, next: express.NextFunction ) => {

  console.log('Check if request is authorized with Firebase ID token');

  if ((!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) &&
    !req.cookies.__session) {
    console.error('No Firebase ID token was passed as a Bearer token in the Authorization header.',
      'Make sure you authorize your request by providing the following HTTP header:',
      'Authorization: Bearer <Firebase ID Token>',
      'or by passing a "__session" cookie.');
    res.status(403).send('Unauthorized');
    return;
  }

  let idToken;
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
    console.log('Found "Authorization" header');
    // Read the ID Token from the Authorization header.
    idToken = req.headers.authorization.split('Bearer ')[ 1 ];
  } else {
    console.log('Found "__session" cookie');
    // Read the ID Token from cookie.
    idToken = req.cookies.__session;
  }
  admin.auth().verifyIdToken(idToken).then(( decodedIdToken ) => {
    console.log('ID Token correctly decoded', decodedIdToken);
    req.user = decodedIdToken;
    return next();
  }).catch(( error ) => {
    console.error('Error while verifying Firebase ID token:', error);
    res.status(403).send('Unauthorized');
  });
};

app.use(cors);
app.use(cookieParser);
app.use(validateFirebaseIdToken);
app.use(bodyParser.json());
// app-app/remove-account
app.post('/remove-account', ( req: any, res: any ) => {
  const loggedUser = req.user;
  const uid: string = req.body.uid;

  if (loggedUser.uid !== uid) {
    res.status(403).send('Unauthorized');
    return;
  }

  admin.auth().deleteUser(uid)
    .then(() => {
      res.status(200).send();
    })
    .catch(( err ) => {
      res.status(500).send(err);
    });
});

// app-app/subscribe-to-topic
app.post('/subscribe-to-topic', ( req: any, res: any ) => {
  const topic: string = req.body.topic;
  const tokens: string[] = req.body.tokens;
  admin.messaging().subscribeToTopic(tokens, topic)
    .then(( data: admin.messaging.MessagingTopicManagementResponse ) => {
      console.log('subscribed to topic', data);
      res.status(200).send();
    })
    .catch(( err ) => {
      res.status(500).send(err);
    });

});

// app-app/unsubscribe-from-topic
app.post('/unsubscribe-from-topic', ( req: any, res: any ) => {
  const topic: string = req.body.topic;
  const tokens: string[] = req.body.tokens;
  admin.messaging().unsubscribeFromTopic(tokens, topic)
    .then(( data: admin.messaging.MessagingTopicManagementResponse ) => {
      console.log('unsubscribed from topic', data);
      res.status(200).send();
    })
    .catch(( err ) => {
      res.status(500).send(err);
    });

});

// app-app/transaction
app.post('/transaction', ( req: any, res: any ) => {
  const user: FirebaseUser = req.user;
  const productId: string = req.body.productId;
  const paymentId: string = req.body.paymentId;

  if (!productId || !paymentId || !user.uid) {
    res.status(403);
  }
  prepareTransactionBody(user, res, productId, paymentId);
});

function prepareTransactionBody( firebaseUser: FirebaseUser, res: any, productId: string, paymentId: string ): void {


  db.doc(`users/${firebaseUser.uid}/private/user`)
    .get()
    .then(( userSnapshot: DocumentSnapshot ) => {
      return <User>userSnapshot.data();
    })
    .then(( user: User ) => {
      db.doc(`payments/${user.uid}/payments/${paymentId}`)
        .get()
        .then(( documentSnapshot: DocumentSnapshot ) => {
          const paymentData: PaymentData = <PaymentData>documentSnapshot.data();
          const sessionId: string = `${user.uid}-${productId}-${paymentId}`;
          const transactionBody: TransactionBody = {
            p24_merchant_id: MERCHANT_ID,
            p24_pos_id: MERCHANT_ID,
            p24_session_id: sessionId,
            p24_amount: <number>(paymentData.price * 100),
            p24_currency: 'PLN',
            p24_description: `Platnosc ${paymentId}`,
            p24_email: user.email,
            p24_client: user.firstName + ' ' + user.lastName,
            p24_address: user.address.street,
            p24_city: user.address.city,
            p24_zip: user.address.zip,
            p24_country: 'PL',
            p24_phone: user.phone,
            p24_language: 'pl',
            p24_url_return: `https://ogar-2018.firebaseapp.com/platnosc/${paymentId}`,
            p24_url_status: 'https://us-central1-ogar-2018.cloudfunctions.net/confirm/transaction',
            p24_shipping: 0,
            p24_api_version: '3.2',
            p24_sign: md5(`${sessionId}|${MERCHANT_ID}|${paymentData.price * 100}|PLN|${CRC}`),
          };

          paymentData.items.forEach(( d: PaymentItem, i: number ) => {
            transactionBody[ `p24_name_${i + 1}` ] = d.name;
            transactionBody[ `p24_quantity_${i + 1}` ] = 1;
            transactionBody[ `p24_price_${i + 1}` ] = d.price * 100;

          });

          request({
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
            },
            uri: 'https://sandbox.przelewy24.pl/trnRegister', // todo zmienic na produkcje
            form: transactionBody,
            method: 'POST',
          }, ( err, response, body ) => {

            if (err) {
              console.log(err);
              return res.status(500).send(err);
            }
            const p24response = querystring.parse(body);
            res.status(200).send({response: p24response});
          });

        })
        .catch(( err ) => {
          console.log(err);
          res.status(500).send(err);
        });

    })
    .catch(( err ) => {
      res.status(500).send(err);
    });

}

exports.app = functions.https.onRequest(app);
