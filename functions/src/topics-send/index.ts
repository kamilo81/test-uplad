import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { DocumentSnapshot } from 'firebase-functions/lib/providers/firestore';
import { EventContext } from 'firebase-functions';

export const topicsSend = functions.firestore
  .document(`topics/{topic}/userMessages/{messageId}`)
  .onCreate((snapshot: DocumentSnapshot, context: EventContext) => {
    const message = <{ title: string; body: string; }>snapshot.data();
    const topic = context.params.topic;
    const payload = {
      notification: {
        title: message.title,
        body: message.body,
      },
    };

    return admin.messaging().sendToTopic(topic, payload)
      .then(( response: admin.messaging.MessagingTopicResponse ) => {
        console.log('Successfully sent message:', response);
      })
      .catch(( error ) => {
        console.log('Error sending message:', error);
      });

  });
