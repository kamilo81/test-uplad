import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as admin from 'firebase-admin';
import DocumentSnapshot = admin.firestore.DocumentSnapshot;
import { EventContext } from 'firebase-functions';
import { Item, ItemUnit, UserReservation } from '../../../src/app/shared/model/item.model';
import { User } from '../../../src/app/shared/model/user.model';
import moment = require('moment');

const SENDGRID_API_KEY = functions.config().sendgrid.key;

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(SENDGRID_API_KEY);
sgMail.setSubstitutionWrappers('{{', '}}');

export const putUpItemConfirm = functions.firestore.document(`items/{itemId}`).onCreate((snapshot: DocumentSnapshot, context: EventContext) => {

  const item: Item = snapshot.data();

  return admin.firestore().doc(`users/${item.userUid}/private/user`)
    .get()
    .then((doc) => {
      const user: User = doc.data();
      const msg = {
        to: user.email,
        from: 'nieogar.app@gmail.com',
        subject: 'Wystawiłeś nowy przedmiot',
        templateId: '78375f0a-d0f3-48cf-8557-b6719beab28b',
        substitutions: {
          name: `${user.firstName} ${user.lastName}`,
          itemName: item.name,
          itemSlug: item.slug,
        },
      };

      return sgMail.send(msg);

    })
    .catch((err) => {
      console.log(err);
    });
});

export const reservationConfirm = functions.firestore.document(`reservations/{reservationId}`).onCreate((snapshot: DocumentSnapshot, context: EventContext) => {

  const reservation: UserReservation = snapshot.data();
  let diff: number;
  let dateFrom;
  let dateTo;

  if (reservation.item.unit === ItemUnit.DAY) {
    diff = moment(reservation.range.to).diff(reservation.range.from, 'day') + 1;
    dateFrom = moment(reservation.range.from).format('DD-MM-YYYY');
    dateTo = moment(reservation.range.to).format('DD-MM-YYYY');
  } else {
    diff =  moment(reservation.range.to).diff(reservation.range.from, 'hour') + 1;
    dateFrom = moment(reservation.range.from).format('DD-MM-YYYY HH:mm');
    dateTo = moment(reservation.range.to).add('59', 'minutes').format('DD-MM-YYYY HH:mm');
  }

  return Promise.all([
    admin.firestore().doc(`users/${reservation.lentByUserUid}/private/user`).get(),
    admin.firestore().doc(`users/${reservation.ownerUid}/private/user`).get(),
  ])
    .then(([docUser, docOwner]) => {
      const user: User = docUser.data();
      const owner: User = docOwner.data();
      const msgToUser = {
        to: user.email,
        from: 'nieogar.app@gmail.com',
        subject: `Potwierdzenie rezerwacji`,
        templateId: '63a23eb9-0371-45db-b6b4-d92159176589',
        substitutions: {
          ownerName: `${owner.firstName} ${owner.lastName}`,
          ownerAddress1: `${owner.address.street}`,
          ownerAddress2: `${owner.address.zip} ${owner.address.city}, ${owner.address.country}`,
          ownerMail: owner.email,
          ownerPhone: owner.phone,
          itemName: reservation.item.name,
          reservationDate: moment(reservation.reservationDate).format('DD-MM-YYYY HH:mm'),
          reservationId: reservation.uid,
          reservationFrom: dateFrom,
          reservationTo: dateTo,
          reservationPeriod: `${diff}${reservation.item.unit === ItemUnit.DAY ? 'd' : 'h'}`,
          reservationPrice: `${(reservation.item.unitPrice * diff).toFixed(2)} PLN`,
          itemSlug: reservation.item.slug,
        },
      };

      const msgToOwner = {
        to: owner.email,
        from: 'nieogar.app@gmail.com',
        subject: `Potwierdzenie wypożyczenia przedmiotu`,
        templateId: 'b00ff252-3e23-4819-b576-428fa00ac9f7',
        substitutions: {
          userName: `${user.firstName} ${user.lastName}`,
          userAddress1: `${user.address.street}`,
          userAddress2: `${user.address.zip} ${user.address.city}, ${user.address.country}`,
          userMail: user.email,
          userPhone: user.phone,
          itemName: reservation.item.name,
          reservationDate: moment(reservation.reservationDate).format('DD-MM-YYYY HH:mm'),
          reservationId: reservation.uid,
          reservationFrom: dateFrom,
          reservationTo: dateTo,
          reservationPeriod: `${diff}${reservation.item.unit === ItemUnit.DAY ? 'd' : 'h'}`,
          reservationPrice: `${(reservation.item.unitPrice * diff).toFixed(2)}PLN`,
          itemSlug: reservation.item.slug,
        },
      };

      return Promise.all([sgMail.send(msgToUser), sgMail.send(msgToOwner)]);

    })
    .catch((err) => {
      console.log(err);
    });
});
