import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import UserRecord = admin.auth.UserRecord;
import { User } from '../../../src/app/shared/model/user.model';
import { EventContext } from 'firebase-functions';

const firestore = admin.firestore();
const bucket = admin.storage().bucket();
export const createUserRecord = functions.auth.user()
  .onCreate(( user: UserRecord, context?: EventContext ) => {
    const metadata: admin.auth.UserMetadata = user.metadata;
    const newUserPublic: User = {
      'uid': user.uid,
      'firstName': user.displayName ? user.displayName : '',
      'lastName': '',
      'phone': user.phoneNumber ? user.phoneNumber : '',
      'photoUrl': user.photoURL ? user.photoURL : '',
      'createdTime': new Date(metadata.creationTime),
      'lastSignInTime': new Date(),
      'deletedTime': null,
      'ready': false,
    };

    const newUser: User = {
      ...newUserPublic,
      'email': user.email ? user.email : '',
      'permission': {
        user: true,
        admin: false,
      },
      'address': {
        'street': '',
        'zip': '',
        'city': '',
        'country': '',
      },
    };

    return firestore.doc(`users/${user.uid}`).set(newUserPublic)
      .then(() => {
        firestore.doc(`users/${user.uid}`).collection('private').doc('user').set(newUser);
      });
  });
export const deleteUserRecord = functions.auth.user()
  .onDelete((user: UserRecord, context?: EventContext) => {
    const uid = user.uid;
    bucket.deleteFiles({
      prefix: `users/${uid}/`,
    }); // todo czy usuwać dane usera ????? tak - dla dev
    return firestore.doc(`users/${uid}`).update({deletedTime: new Date()})
      .then(() => {
        firestore.doc(`users/${uid}/private/user`).update({deletedTime: new Date()});
      });
  });
