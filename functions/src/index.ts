import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp(functions.config().firebase);

import * as auth from './auth-record';
import * as fcm from './fcm-send';
import * as topics from './topics-send';
import * as user from './user';
import * as algolia from './instant-search';
import * as http from './http';
import * as thumbs from './generate-thumbnail';
import * as sendgrid from './send-mail';
import * as p24confirm from './confirmTransaction';

export const createUser =  auth.createUserRecord;
export const deleteUser =  auth.deleteUserRecord;
export const fcmSend =  fcm.fcmSend;
export const topicsSend =  topics.topicsSend;
export const userPresence =  user.userPresence;
export const updateIndex =  algolia.updateIndex;
export const deleteIndex =  algolia.deleteIndex;
export const generateThumbnail =  thumbs.generateThumbnail;
export const app = http;
export const confirm = p24confirm.transaction;
export const putUpItemConfirm = sendgrid.putUpItemConfirm;
export const reservationConfirm = sendgrid.reservationConfirm;

