import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
export const IMAGES_LIMIT = 6;
import * as LoadImage from '../assets/js/load-image/index.js';
import {DomSanitizer} from '@angular/platform-browser';
import {Subject, throwError} from 'rxjs';
import {AngularFireStorage, AngularFireUploadTask} from 'angularfire2/storage';
import {catchError, finalize, take, takeUntil} from 'rxjs/operators';
import {AngularFirestore} from 'angularfire2/firestore';
export function toDataUrl(image: HTMLCanvasElement, fileExtension: string): string {
  return image.toDataURL(fileExtension.toLowerCase() === 'png' ? 'image/png' : 'image/jpeg', 1);
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public tasks: any[] = [];
  @ViewChild('myInput') public myInputVariable: ElementRef;
  public sizeLimit = 10;
  public imagesPreviewFromDb: any[] = [];
  public files: File[] = [];
  public imagesPreview: any[] = [];
  public componentTemplateList: any[] = [];
  public loadingigImages: any[] = [];
  constructor(private ngZone: NgZone,
              private afs: AngularFirestore,
              private storage: AngularFireStorage,
              private sanitizer: DomSanitizer) {
  }

  public ngOnInit(): void {
    this.sizeLimit = this.sizeLimit * 1024 * 1024;
  }

  public uploadFile(event: any): void {
    if (event.target['files'].length <= 0) {
      return;
    }
    const removeValFromIndex: number[] = [];
    const files: File[] = this.toArray(event.target['files']);

    files.forEach((file: File, index: number) => {
      if (file.size > this.sizeLimit) {
        console.log('too big ', file.name);
        removeValFromIndex.push(index);
      }
    });

    for (let i = removeValFromIndex.length - 1; i >= 0; i--) {
      files.splice(removeValFromIndex[i], 1);
    }
    this.onUpload(files);
    this.myInputVariable.nativeElement.value = '';
  }

  public onUpload( files: File[] ): void {

    const imagesCounter: number = this.files.length + files.length + this.imagesPreviewFromDb.length;
    const uploadedImages = files;
    if (imagesCounter > IMAGES_LIMIT) {
      const tooMuch: number = imagesCounter - IMAGES_LIMIT;
      uploadedImages.splice(uploadedImages.length - tooMuch, tooMuch);
    }

    this.files = this.files.concat(uploadedImages);
    this.files = this.sortImages(this.files);
    this.previewImages(this.files);
  }

  private toArray(File: File[]): File[] {
    return Array.prototype.slice.call(File);
  }
  private sortImages( images: any ): any {
    const byName: any[] = images.slice(0);

    return byName.sort(( a, b ) => {
      const x: string = a.name.toLowerCase();
      const y: string = b.name.toLowerCase();
      return x < y ? -1 : x > y ? 1 : 0;
    });
  }

  private previewImages( files: File[] ): void {
    this.previewImagesService(files, 1200, 1200)
      .then(( imagesPreview: any[] ) => {
        this.imagesPreview = this.sortImages([ ...imagesPreview ]);
        this.setupImages();
      })
      .catch(( error: any ) => {
        console.log(error);
      });
  }

  private setupImages( fromDb: boolean = false ): void {
    this.componentTemplateList = [];
    this.imagesPreview.forEach(( image: any, index: number ) => {
      const componentTemplate: any = {
        data: {
          index: index,
          image: image.items[ 'original' ],
          orientation: image.orientation,
        },
      };
      this.componentTemplateList.push(componentTemplate);
    });
  }

  public previewImagesService(files: File[], maxWidth: number = 828, maxHeight: number = null, crop: boolean = false): Promise<any[]> {
    const imagesPreview: any[] = [];
    this.loadingigImages = [];

    return new Promise((resolve) => {
      for (const file of files) {
        if (!file.type.match('image.*')) {
          continue;
        }
        this.ngZone.run(() => {
          const loadingImage = LoadImage(file, (img: any, meta: any) => {
              const items: any = {};
              items['original'] = img;

              imagesPreview.push({
                name: file.name,
                items,
                orientation: meta && meta.exif ? meta.exif.get('Orientation') : null,
              });

              if (imagesPreview.length === files.length) {
                resolve(imagesPreview);
              }
            }, {
              maxWidth,
              maxHeight: maxHeight,
              meta: true,
              crop,
              canvas: true,
              orientation: true,
              pixelRatio: window.devicePixelRatio,
              crossOrigin: 'Anonymous',
            }
          );
          this.loadingigImages.push(this.sanitizer.bypassSecurityTrustUrl(loadingImage.src));
        });
      }
    });
  }

  public save(): void {
    const directory = `items/1`;
    const fileName = `test`;

    this.uploadImages(this.files, directory, fileName, 0)
      .then(( images: any[] ) => {
        this.files = [];
        this.loadingigImages = [];
        this.imagesPreview = [];
        this.componentTemplateList = [];
        this.tasks = [];
      });
  }

  public uploadImages(files: File[], directory: string, fileName: string, orderStart: number = 0, showProgress: boolean = true): Promise<any> {
    const images: any[] = [];
    let errors = 0;
    const resolver$: Subject<void> = new Subject();
    if (files.length <= 0) {
      return new Promise((resolve => resolve([])));
    }
    let i: number = orderStart;

    // return this.previewImagesService(files, 1200, 1200)
    //   .then((imgsObj: any[]) => {
    //   });

        return <Promise<any[]>>new Promise((resolve) => {
          for (const file of files) {
            const id: string = this.afs.createId();
            const newFilename = `${i}__${fileName}++${id}`;

            const fileExtension: string = file.name.split('.').reverse()[0];
            const path = `${directory}/${newFilename ? newFilename + '.' + fileExtension : file.name}`;

            // const task: AngularFireUploadTask = this.uploadFileService(toDataUrl((<HTMLCanvasElement>file.items.original), fileExtension), path);
            const task: AngularFireUploadTask = this.uploadFileService(file, path);
            task.percentageChanges()
              .subscribe((percent: number) => {
                console.log(percent);
              });
            this.tasks.push(task.percentageChanges());
            task.snapshotChanges()
              .pipe(
                catchError((err, caught) => {
                  console.log('3 ', err);
                  console.log('3 ', caught);
                  errors++;
                  return throwError({ err });
                }),
                finalize(() => {
                  this.getDownloadUrl(path)
                    .then((downloadUrl: string) => {
                      console.log(downloadUrl);
                      const items: any = {};
                      items['original'] = downloadUrl;
                      images.push({
                        id: id,
                        defaultImg: !images.length,
                        items,
                      });

                      if (images.length + errors === files.length) {
                        resolver$.next();
                        resolver$.complete();
                        resolve(images);
                        console.log('end');
                      }
                    })
                    .catch((err) => {
                      console.log(err);
                      if (images.length + errors === files.length) {
                        resolver$.next();
                        resolver$.complete();
                        resolve(images);
                      }
                    });
                }),
                takeUntil(resolver$)
              )
              .subscribe(() => {
              });



            i++;
          }
        });

  }

  // public uploadFileService(file: string, path: string): AngularFireUploadTask {
  //   return this.storage.ref(path).putString(file, 'data_url');
  //   // return this.storage.ref(path).putString(file.split(',')[1], 'base64');
  // }

  public uploadFileService(file: File, path: string): AngularFireUploadTask {
    console.log('file ', file);
    return this.storage.upload(path, file);
  }

  public getDownloadUrl(path: string): Promise<string> {
    return this.storage.ref(path).getDownloadURL()
      .pipe(
        take(1)
      )
      .toPromise();
  }
}
